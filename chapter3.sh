# !/usr/bin/env bash
#############################
#owner: droritzz
#puprose: answering questions from the LPIC1
#date: 11/10/20
###########################

#1. Use the jobs command to verify whether you have any processes running in background.
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]-  Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]+  Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
student@vaiolabs:~$

#2. Use vi to create a little text file. Suspend vi in background.

Debian10:
student@vaiolabs:~$ vi textfile.tx
############ PRESS CTRL+Z ########
[9]+  Stopped                 vi textfile.tx
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]-  Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]+  Stopped                 vi textfile.tx

CentOS8:
[student@student ~]$ vi textfile.txt

[3]+  Stopped                 vim textfile.txt
[student@student ~]$ jobs
[1]   Running                 echo AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[2]-  Running                 cat < pipe2 > pipe3 &  (wd: ~/pipes)
[3]+  Stopped                 vim textfile.txt

#3. Verify with jobs that vi is suspended in background.

#4. Start find / > allfiles.txt 2>/dev/null in foreground. Suspend it in background before it finishes.
student@vaiolabs:~$ find / > allfiles.txt 2> /dev/null
^Z
[10]+  Stopped                 find / > allfiles.txt 2> /dev/null


#5. Start two long sleep processes in background.
student@vaiolabs:~$ sleep 1000000 &
[11] 8535
student@vaiolabs:~$ sleep 1000001 &
[12] 8536

#6. Display all jobs in background.
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]   Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]-  Stopped                 vi textfile.tx
[10]+  Stopped                 find / > allfiles.txt 2> /dev/null
[11]   Running                 sleep 1000000 &
[12]   Running                 sleep 1000001 &

#7. Use the kill command to suspend the last sleep process.
student@vaiolabs:~$ kill -15 8536
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]   Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]-  Stopped                 vi textfile.tx
[10]+  Stopped                 find / > allfiles.txt 2> /dev/null
[11]   Running                 sleep 1000000 &
[12]   Terminated              sleep 1000001
student@vaiolabs:~$ kill -19 8535
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]   Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]   Stopped                 vi textfile.tx
[10]-  Stopped                 find / > allfiles.txt 2> /dev/null
[11]+  Stopped                 sleep 1000000


#8. Continue the find process in background (make sure it runs again).
student@vaiolabs:~$ bg 10
[10] find / > allfiles.txt 2> /dev/null &
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]   Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]+  Stopped                 vi textfile.tx
[10]   Exit 1                  find / > allfiles.txt 2> /dev/null
[11]-  Stopped                 sleep 1000000

#9. Put one of the sleep commands back in foreground.

student@vaiolabs:~$ sleep 500 &
[12] 8561
student@vaiolabs:~$ jobs
[3]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[4]   Running                 echo -n AAA | cat - pipe1 > pipe2 &  (wd: ~/pipes)
[5]   Running                 echo -n AAA | cat - pipe2 > pipe3 &  (wd: ~/pipes)
[6]   Running                 cat < pipe3 > pipe1 &  (wd: ~/pipes)
[7]   Running                 echo -n BBB | nice cat - pipe4 > pipe5 &  (wd: ~/pipes)
[8]   Running                 nice cat < pipe5 > pipe6 &  (wd: ~/pipes)
[9]+  Stopped                 vi textfile.tx
[11]-  Stopped                 sleep 1000000
[12]   Running                 sleep 500 &
student@vaiolabs:~$ fg 12
sleep 500


