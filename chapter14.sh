# !/usr/bin/env bash
#############################
#owner: droritzz
#puprose: answering questions from the LPIC1
#date: 14/11/20
###########################

#0. Find out whether your system is using lilo, grub or grub2. Only do the practices that are appropriate for your system.
Debian 10 - grub
student@vaiolabs:~$ cd /boot
student@vaiolabs:/boot$ ls
config-4.19.0-8-amd64  initrd.img-4.19.0-8-amd64  vmlinuz-4.19.0-8-amd64
grub                   System.map-4.19.0-8-amd64
student@vaiolabs:/boot$

CentOS 7 - grub and grub2
[root@student student]# cd /boot
[root@student boot]# ls
config-3.10.0-1127.el7.x86_64
efi
grub
grub2
initramfs-0-rescue-6c5e84227dbd4529b447bb985168b57b.img
initramfs-3.10.0-1127.el7.x86_64.img
symvers-3.10.0-1127.el7.x86_64.gz
System.map-3.10.0-1127.el7.x86_64
vmlinuz-0-rescue-6c5e84227dbd4529b447bb985168b57b
vmlinuz-3.10.0-1127.el7.x86_64
[root@student boot]#


#1. Make a copy of the kernel, initrd and System.map files in /boot. Put the copies also in /boot but replace 2.x or 3.x with 4.0 (just imagine that Linux 4.0 is out.).

Debian 10
root@vaiolabs:/boot# cp System.map-4.19.0-8-amd64 System.map-4.0
root@vaiolabs:/boot# cp initrd.img-4.19.0-8-amd64 initrd.img-4.0
root@vaiolabs:/boot# cp vmlinuz-4.19.0-8-amd64 vmlinuz-4.0
root@vaiolabs:/boot# ls
config-4.19.0-8-amd64  initrd.img-4.0		  System.map-4.0	     vmlinuz-4.0
grub		       initrd.img-4.19.0-8-amd64  System.map-4.19.0-8-amd64  vmlinuz-4.19.0-8-amd64
root@vaiolabs:/boot#


CentOS7
[root@student boot]# cp initramfs-3.10.0-1127.el7.x86_64.img initramfs-4.0.img
[root@student boot]# cp System.map-3.10.0-1127.el7.x86_64 System.map-4.0
[root@student boot]# cp vmlinuz-3.10.0-1127.el7.x86_64 vmlinuz-4.0


#2. Add a stanza in grub for the 4.0 files. Make sure the title is different.
Debian 10:
root@vaiolabs:/etc/grub.d# vi 40_custom 
root@vaiolabs:/etc/grub.d# update-grub


#3. Set the boot menu timeout to 30 seconds.
Debian 10:
root@vaiolabs:~# cd /etc/default/
root@vaiolabs:/etc/default# ls
acpid		 avahi-daemon  cacerts	      cryptdisks  hddtemp	   irqbalance  nss	saned
acpi-support	 bluetooth     console-setup  dbus	  hwclock	   keyboard    openvpn	timeshift.json
amd64-microcode  brltty        crda	      grub	  im-config	   locale      rsync	ufw
anacron		 bsdmainutils  cron	      grub.d	  intel-microcode  networking  rsyslog	useradd
root@vaiolabs:/etc/default# vi grub
root@vaiolabs:/etc/default# update-grub
Generating grub configuration file ...
Found theme: /boot/grub/themes/linuxmint/theme.txt
Found linux image: /boot/vmlinuz-4.19.0-8-amd64
Found initrd image: /boot/initrd.img-4.19.0-8-amd64
Found linux image: /boot/vmlinuz-4.0
Found initrd image: /boot/initrd.img-4.0
done
root@vaiolabs:/etc/default# 


CentOS 7
[root@student ~]# cd /boot/grub2
[root@student grub2]# ls
device.map  fonts  grub.cfg  grubenv  i386-pc  locale
[root@student grub2]# vi grub.cfg
[root@student grub2]# 


#4. Reboot and test the new stanza.
