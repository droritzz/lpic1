# !/usr/bin/env bash
#############################
#owner: droritzz
#puprose: answering questions from the LPIC1
#date: 11/10/20
###########################

#1. Create a new directory and create six pipes in that directory.
Debian10:
student@vaiolabs:~$ ls
Desktop  Downloads  homework  Projects
student@vaiolabs:~$ mkdir pipes
student@vaiolabs:~$ cd pipes/
student@vaiolabs:~/pipes$ mkfifo pipe{1..6}
student@vaiolabs:~/pipes$ ls
pipe1  pipe2  pipe3  pipe4  pipe5  pipe6

#2. Bounce a character between two pipes.
Debian10:
student@vaiolabs:~/pipes$ echo -n AAA |cat - pipe1 > pipe2 &
[4] 8355
student@vaiolabs:~/pipes$ echo -n AAA |cat - pipe2 > pipe3 &
[5] 8358
student@vaiolabs:~/pipes$ cat <pipe3 >pipe1 &
[6] 8359
student@vaiolabs:~/pipes$

CentOS8:
student@student ~]$ ls
Desktop  Downloads  homework  Projects
[student@student ~]$ mkdir pipes
[student@student ~]$ cd pipes/
[student@student pipes]$ mkfifo pipe{1..6}
[student@student pipes]$ ls
pipe1  pipe2  pipe3  pipe4  pipe5  pipe6
[student@student pipes]$ echo AAA | cat - pipe1 > pipe2 &
[1] 5644
[student@student pipes]$ cat <pipe2 >pipe3 &
[2] 5659
[student@student pipes]$

#3. Use top and ps to display information (pid, ppid, priority, nice value, ...) about these two cat processes.
Debian10:
student@vaiolabs:~/pipes$ top

top - 13:04:44 up  5:12,  1 user,  load average: 2.59, 2.04, 1.06
Tasks: 156 total,   2 running, 154 sleeping,   0 stopped,   0 zombie
%Cpu(s): 16.6 us, 83.4 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :   3947.0 total,   1898.1 free,    713.0 used,   1335.8 buff/cache
MiB Swap:   7476.0 total,   7476.0 free,      0.0 used.   2921.0 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                   
 8359 student   20   0    5400    684    620 R  26.7   0.0   2:07.91 cat                                       
 8358 student   20   0    5400   1696   1612 S  24.0   0.0   1:56.51 cat                                       
 8353 student   20   0    5400   1684   1600 S  21.0   0.0   1:41.51 cat                                       
 8355 student   20   0    5400   1556   1472 S  21.0   0.0   1:41.56 cat       

student@vaiolabs:~/pipes$ ps fax | grep cat
 1334 ?        Sl     0:00          \_ /usr/lib/x86_64-linux-gnu/cinnamon-settings-daemon/csd-print-notifications
 1511 ?        Sl     0:00          \_ /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
 8353 pts/1    S      2:27          \_ cat - pipe1
 8355 pts/1    S      2:27          \_ cat - pipe1
 8358 pts/1    S      2:49          \_ cat - pipe2
 8359 pts/1    R      3:05          \_ cat
 8382 pts/1    S+     0:00          \_ grep cat
student@vaiolabs:~/pipes$ 


CentOS8:
[student@student pipes]$ ps fax | grep cat
   1649 tty1     Sl+    0:00  |           \_ /usr/libexec/gsd-print-notifications
   2192 tty2     Sl+    0:00              \_ /usr/libexec/gsd-print-notifications
   2443 tty2     Sl+    0:03              \_ /usr/bin/gnome-software --gapplication-service
   5644 pts/0    S      0:00          \_ cat - pipe1
   5815 pts/0    S+     0:00          \_ grep --color=auto cat

##########
in CentOS8 you have to run the top -p command with the PID to see the process
[student@student pipes]$ top -p 5644

top - 13:32:04 up  4:55,  1 user,  load average: 0.00, 0.00, 0.00
Tasks:   1 total,   0 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s):  5.1 us,  0.7 sy,  0.0 ni, 93.5 id,  0.0 wa,  0.7 hi,  0.0 si,  0.0 st
MiB Mem :   3939.0 total,   1949.2 free,   1127.9 used,    861.9 buff/cache
MiB Swap:   3280.0 total,   3280.0 free,      0.0 used.   2561.7 avail Mem

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
   5644 student   20   0    7284   1888   1788 S   0.0   0.0   0:00.00 cat
#4. Bounce another character between two other pipes, but this time start the commands nice.

student@vaiolabs:~/pipes$ echo -n BBB | nice cat - pipe4 > pipe5 &
[7] 8443
student@vaiolabs:~/pipes$ nice cat <pipe5 > pipe6 &
[8] 8446

#Verify that all cat processes are battling for the cpu. (Feel free to fire up two more cats with the remaining pipes).

student   8353 21.4  0.0   5400  1684 pts/1    S    12:55  12:25 cat - pipe1
student   8355 21.5  0.0   5400  1556 pts/1    S    12:55  12:27 cat - pipe1
student   8358 24.9  0.0   5400  1696 pts/1    S    12:56  14:15 cat - pipe2
student   8359 27.6  0.0   5400   684 pts/1    R    12:56  15:37 cat
student   8443  0.0  0.0   5264  1696 pts/1    SN   13:41   0:00 cat - pipe4

#5. Use ps to verify that the two new cat processes have a nice value. Use the -o and -C options of ps for this.
student@vaiolabs:~/pipes$ ps -C cat -o pid,ppid,ni,comm
  PID  PPID  NI COMMAND
 8353  7461   0 cat
 8355  7461   0 cat
 8358  7461   0 cat
 8359  7461   0 cat
 8443  7461   5 cat

#6. Use renice te increase the nice value from 10 to 15. Notice the difference with the usual commands

student@vaiolabs:~/pipes$ sudo renice +5 8443
[sudo] password for student:
8443 (process ID) old priority 10, new priority 5
student@vaiolabs:~/pipes$

student@vaiolabs:~/pipes$ ps -C cat -o pid,ppid,ni,comm
  PID  PPID  NI COMMAND
 8353  7461   0 cat
 8355  7461   0 cat
 8358  7461   0 cat
 8359  7461   0 cat
 8443  7461   5 cat



