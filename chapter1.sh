# !/usr/bin/env bash
#############################
#owner: droritzz
#puprose: answering questions from the LPIC1
#date: 11/10/20
###########################
#1. Use ps to search for the init process by name.
student@vaiolabs:~$ man ps
student@vaiolabs:~$ ps -C init
  PID TTY          TIME CMD

#2. What is the process id of the init process ?
#it's always 1

#3. Use the who am i command to determine your terminal name.
debian 10
student@vaiolabs:~$ tty
/dev/pts/1
student@vaiolabs:~$ 

CentOS8
[student@student ~]$ tty
/dev/pts/0

#4. Using your terminal name from above, use ps to find all processes associated with your terminal
Debian 10:
student@vaiolabs:~$ ps fax |grep pts/1
 7461 pts/1    Ss     0:00      \_ bash
 7987 pts/1    R+     0:00          \_ ps fax
 7988 pts/1    S+     0:00          \_ grep pts/1

CentOS8:
[student@student ~]$ man ps
[student@student ~]$ ps fax |grep /pts/0
   4947 pts/0    S+     0:00          \_ grep --color=auto /pts/0

#5. What is the process id of your shell ?
Debian 10:
student@vaiolabs:~$ ps fax | grep bash
 1795 pts/0    Ss     0:00      \_ bash
 1841 pts/0    S      0:00      |       \_ bash
 7461 pts/1    Ss     0:00      \_ bash
 8210 pts/1    S+     0:00          \_ grep bash

 7461

CentOS8:
[student@student ~]$ ps fax |grep bash
    992 ?        S      0:00 /bin/bash /usr/sbin/ksmtuned
   2622 pts/0    Ss     0:00      \_ bash
   4982 pts/0    S+     0:00          \_ grep --color=auto bash

   2622

#6. What is the parent process id of your shell ?
Debian10:
1795
CentOS8:
992

#7. Start two instances of the sleep 3342 in background.
student@vaiolabs:~$ sleep 3342 &
[1] 8215
student@vaiolabs:~$ sleep 3342 &
[2] 8216

#8. Locate the process id of all sleep commands.
student@vaiolabs:~$ pidof sleep
8216 8215

#9. Display only those two sleep processes in top. Then quit top.

Debian10:
student@vaiolabs:~$ top -p 8216, 8215
 8216 student   20   0    5260    684    620 S   0.0   0.0   0:00.00 sleep
 8215 student   20   0    5260    744    680 S   0.0   0.0   0:00.00 sleep

CenOS8
[student@student ~]$ top -p 5033, 5040

top - 12:19:34 up  3:42,  1 user,  load average: 0.02, 0.01, 0.00
Tasks:   2 total,   0 running,   2 sleeping,   0 stopped,   0 zombie
%Cpu(s): 15.7 us,  3.1 sy,  0.0 ni, 79.1 id,  0.0 wa,  2.1 hi,  0.0 si,  0.0 st
MiB Mem :   3939.0 total,   1954.7 free,   1125.1 used,    859.1 buff/cache
MiB Swap:   3280.0 total,   3280.0 free,      0.0 used.   2566.2 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND  
   5033 student   20   0    7280    828    764 S   0.0   0.0   0:00.00 sleep    
   5040 student   20   0    7280    776    708 S   0.0   0.0   0:00.00 sleep 

#10. Use a standard kill to kill one of the sleep processes.
student@vaiolabs:~$ kill -15 8216

#11. Use one command to kill all sleep processes.
Debian10:
student@vaiolabs:~$ jobs
[1]-  Running                 sleep 3342 &
[2]+  Terminated              sleep 3342
student@vaiolabs:~$ killall sleep
[1]+  Terminated              sleep 3342
student@vaiolabs:~$ jobs

CentOS8:
[student@student ~]$ kill -15 5033
[1]-  Terminated              sleep 3342
[student@student ~]$ jobs
[2]+  Running                 sleep 3342 &
[student@student ~]$ killall sleep
[2]+  Terminated              sleep 3342

