#!/usr/bin/env bash

##################################
#owner: droritzz
#purpose: homework practice 
#date : 3/11/2020
#################################

#verify wherether gcc sudo and wesnoth are installed.

[root@student ~]# rpm -q sudo
sudo-1.8.29-5.el8.x86_64
[root@student ~]# rpm -q gcc
gcc-8.3.1-5.el8.0.2.x86_64
[root@student ~]# rpm -q wesnoth
package wesnoth is not installed


#use yum or apt-get to search for and install the scp, tmus and man-pages packages.

[root@student ~]# yum search scp
Last metadata expiration check: 1:48:26 ago on Tue 03 Nov 2020 08:03:30 PM IST.
=================================== Name & Summary Matched: scp ===================================
python3-scp.noarch : scp module for paramiko
======================================== Name Matched: scp ========================================
cscppc.x86_64 : A compiler wrapper that runs cppcheck in background
scponly.x86_64 : Restricted shell for ssh based file services
python3-lesscpy.noarch : Lesscss compiler
[root@student ~]# yum search tmux
Last metadata expiration check: 1:48:44 ago on Tue 03 Nov 2020 08:03:30 PM IST.
=================================== Name Exactly Matched: tmux ====================================
tmux.x86_64 : A terminal multiplexer
====================================== Summary Matched: tmux ======================================
xpanes.noarch : Awesome tmux-based terminal divider
[root@student ~]# yum search man-pages
Last metadata expiration check: 1:49:01 ago on Tue 03 Nov 2020 08:03:30 PM IST.
================================= Name Exactly Matched: man-pages =================================
man-pages.x86_64 : Linux kernel and C library user-space interface documentation
===================================== Name Matched: man-pages =====================================
man-pages-overrides.noarch : Complementary and updated manual pages
libguestfs-man-pages-ja.noarch : Japanese (ja) man pages for libguestfs
libguestfs-man-pages-uk.noarch : Ukrainian (uk) man pages for libguestfs


#search the internet for "webmin" and figure out how to install it.

######
#on debian - go to https://www.webmin.com/deb.html for the information.
#on redhat - go to https://www.webmin.com/rpm.html 

####### installation on CentOS 8 ##########

[root@student ~]# wget http://prdownloads.sourceforge.net/webadmin/webmin-1.960-1.noarch.rpm
--2020-11-03 21:56:52--  http://prdownloads.sourceforge.net/webadmin/webmin-1.960-1.noarch.rpm
Resolving prdownloads.sourceforge.net (prdownloads.sourceforge.net)... 216.105.38.13
Connecting to prdownloads.sourceforge.net (prdownloads.sourceforge.net)|216.105.38.13|:80... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: http://downloads.sourceforge.net/project/webadmin/webmin/1.960/webmin-1.960-1.noarch.rpm [following]
--2020-11-03 21:56:58--  http://downloads.sourceforge.net/project/webadmin/webmin/1.960/webmin-1.960-1.noarch.rpm
Resolving downloads.sourceforge.net (downloads.sourceforge.net)... 216.105.38.13
Reusing existing connection to prdownloads.sourceforge.net:80.
HTTP request sent, awaiting response... 302 Found
Location: https://netix.dl.sourceforge.net/project/webadmin/webmin/1.960/webmin-1.960-1.noarch.rpm [following]
--2020-11-03 21:56:59--  https://netix.dl.sourceforge.net/project/webadmin/webmin/1.960/webmin-1.960-1.noarch.rpm
Resolving netix.dl.sourceforge.net (netix.dl.sourceforge.net)... 87.121.121.2
Connecting to netix.dl.sourceforge.net (netix.dl.sourceforge.net)|87.121.121.2|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 40445872 (39M) [application/octet-stream]
Saving to: ‘webmin-1.960-1.noarch.rpm’

webmin-1.960-1.noarch.rp 100%[=================================>]  38.57M   383KB/s    in 82s     

2020-11-03 21:58:22 (480 KB/s) - ‘webmin-1.960-1.noarch.rpm’ saved [40445872/40445872]

[root@student ~]# yum -y install perl perl-Net-SSLeay openssl perl-IO-Tty perl-Encode-Detect
Last metadata expiration check: 1:55:20 ago on Tue 03 Nov 2020 08:03:30 PM IST.
Package perl-Net-SSLeay-1.88-1.el8.x86_64 is already installed.
Package openssl-1:1.1.1c-15.el8.x86_64 is already installed.
Dependencies resolved.
===================================================================================================
 Package                            Architecture Version                    Repository        Size
===================================================================================================
Installing:
 perl                               x86_64       4:5.26.3-416.el8           AppStream         72 k
 perl-Encode-Detect                 x86_64       1.01-28.el8                AppStream         90 k
 perl-IO-Tty                        x86_64       1.12-11.el8                PowerTools        48 k
Installing dependencies:
 perl-Algorithm-Diff                noarch       1.1903-9.el8               AppStream         52 k
 perl-Archive-Tar                   noarch       2.30-1.el8                 AppStream         79 k
 perl-Archive-Zip                   noarch       1.60-3.el8                 AppStream        108 k
 perl-Attribute-Handlers            noarch       0.99-416.el8               AppStream         88 k
 perl-B-Debug                       noarch       1.26-2.el8                 AppStream         26 k
 perl-CPAN                          noarch       2.18-397.el8               AppStream        554 k
 perl-CPAN-Meta                     noarch       2.150010-396.el8           AppStream        191 k
 perl-CPAN-Meta-Requirements        noarch       2.140-396.el8              AppStream         37 k
 perl-CPAN-Meta-YAML                noarch       0.018-397.el8              AppStream         34 k
 perl-Compress-Bzip2                x86_64       2.26-6.el8                 AppStream         72 k
 perl-Compress-Raw-Bzip2            x86_64       2.081-1.el8                AppStream         40 k
 perl-Compress-Raw-Zlib             x86_64       2.081-1.el8                AppStream         68 k
 perl-Config-Perl-V                 noarch       0.30-1.el8                 AppStream         22 k
 perl-DB_File                       x86_64       1.842-1.el8                AppStream         83 k
 perl-Data-OptList                  noarch       0.110-6.el8                AppStream         31 k
 perl-Data-Section                  noarch       0.200007-3.el8             AppStream         30 k
 perl-Devel-PPPort                  x86_64       3.36-5.el8                 AppStream        118 k
 perl-Devel-Peek                    x86_64       1.26-416.el8               AppStream         93 k
 perl-Devel-SelfStubber             noarch       1.06-416.el8               AppStream         75 k
 perl-Devel-Size                    x86_64       0.81-2.el8                 AppStream         34 k
 perl-Digest-SHA                    x86_64       1:6.02-1.el8               AppStream         66 k
 perl-Encode-devel                  x86_64       4:2.97-3.el8               AppStream         39 k
 perl-Env                           noarch       1.04-395.el8               AppStream         21 k
 perl-ExtUtils-CBuilder             noarch       1:0.280230-2.el8           AppStream         48 k
 perl-ExtUtils-Command              noarch       1:7.34-1.el8               AppStream         19 k
 perl-ExtUtils-Embed                noarch       1.34-416.el8               AppStream         79 k
 perl-ExtUtils-Install              noarch       2.14-4.el8                 AppStream         46 k
 perl-ExtUtils-MM-Utils             noarch       1:7.34-1.el8               AppStream         17 k
 perl-ExtUtils-MakeMaker            noarch       1:7.34-1.el8               AppStream        300 k
 perl-ExtUtils-Manifest             noarch       1.70-395.el8               AppStream         37 k
 perl-ExtUtils-Miniperl             noarch       1.06-416.el8               AppStream         76 k
 perl-ExtUtils-ParseXS              noarch       1:3.35-2.el8               AppStream         83 k
 perl-File-Fetch                    noarch       0.56-2.el8                 AppStream         33 k
 perl-File-HomeDir                  noarch       1.002-4.el8                AppStream         61 k
 perl-File-Which                    noarch       1.22-2.el8                 AppStream         23 k
 perl-Filter                        x86_64       2:1.58-2.el8               AppStream         82 k
 perl-Filter-Simple                 noarch       0.94-2.el8                 AppStream         29 k
 perl-IO-Compress                   noarch       2.081-1.el8                AppStream        258 k
 perl-IO-Zlib                       noarch       1:1.10-416.el8             AppStream         80 k
 perl-IPC-Cmd                       noarch       2:1.02-1.el8               AppStream         43 k
 perl-IPC-SysV                      x86_64       2.07-397.el8               AppStream         43 k
 perl-IPC-System-Simple             noarch       1.25-17.el8                AppStream         43 k
 perl-JSON-PP                       noarch       1:2.97.001-3.el8           AppStream         68 k
 perl-Locale-Codes                  noarch       3.57-1.el8                 AppStream        311 k
 perl-Locale-Maketext               noarch       1.28-396.el8               AppStream         99 k
 perl-Locale-Maketext-Simple        noarch       1:0.21-416.el8             AppStream         78 k
 perl-MRO-Compat                    noarch       0.13-4.el8                 AppStream         24 k
 perl-Math-BigInt-FastCalc          x86_64       0.500.600-6.el8            AppStream         27 k
 perl-Math-BigRat                   noarch       0.2614-1.el8               AppStream         40 k
 perl-Memoize                       noarch       1.03-416.el8               AppStream        118 k
 perl-Module-Build                  noarch       2:0.42.24-5.el8            AppStream        273 k
 perl-Module-CoreList               noarch       1:5.20181130-1.el8         AppStream         87 k
 perl-Module-CoreList-tools         noarch       1:5.20181130-1.el8         AppStream         22 k
 perl-Module-Load                   noarch       1:0.32-395.el8             AppStream         19 k
 perl-Module-Load-Conditional       noarch       0.68-395.el8               AppStream         24 k
 perl-Module-Loaded                 noarch       1:0.08-416.el8             AppStream         74 k
 perl-Module-Metadata               noarch       1.000033-395.el8           AppStream         44 k
 perl-Net-Ping                      noarch       2.55-416.el8               AppStream        101 k
 perl-Package-Generator             noarch       1.106-11.el8               AppStream         27 k
 perl-Params-Check                  noarch       1:0.38-395.el8             AppStream         24 k
 perl-Params-Util                   x86_64       1.07-22.el8                AppStream         44 k
 perl-Perl-OSType                   noarch       1.010-396.el8              AppStream         29 k
 perl-PerlIO-via-QuotedPrint        noarch       0.08-395.el8               AppStream         13 k
 perl-Pod-Checker                   noarch       4:1.73-395.el8             AppStream         33 k
 perl-Pod-Html                      noarch       1.22.02-416.el8            AppStream         87 k
 perl-Pod-Parser                    noarch       1.63-396.el8               AppStream        108 k
 perl-SelfLoader                    noarch       1.23-416.el8               AppStream         82 k
 perl-Software-License              noarch       0.103013-2.el8             AppStream        138 k
 perl-Sub-Exporter                  noarch       0.987-15.el8               AppStream         73 k
 perl-Sub-Install                   noarch       0.928-14.el8               AppStream         27 k
 perl-Sys-Syslog                    x86_64       0.35-397.el8               AppStream         50 k
 perl-Test                          noarch       1.30-416.el8               AppStream         89 k
 perl-Test-Harness                  noarch       1:3.42-1.el8               AppStream        279 k
 perl-Test-Simple                   noarch       1:1.302135-1.el8           AppStream        516 k
 perl-Text-Balanced                 noarch       2.03-395.el8               AppStream         58 k
 perl-Text-Diff                     noarch       1.45-2.el8                 AppStream         45 k
 perl-Text-Glob                     noarch       0.11-4.el8                 AppStream         17 k
 perl-Text-Template                 noarch       1.51-1.el8                 AppStream         64 k
 perl-Thread-Queue                  noarch       3.13-1.el8                 AppStream         24 k
 perl-Time-HiRes                    x86_64       1.9758-1.el8               AppStream         61 k
 perl-Time-Piece                    x86_64       1.31-416.el8               AppStream         98 k
 perl-Unicode-Collate               x86_64       1.25-2.el8                 AppStream        686 k
 perl-autodie                       noarch       2.29-396.el8               AppStream         98 k
 perl-bignum                        noarch       0.49-2.el8                 AppStream         44 k
 perl-devel                         x86_64       4:5.26.3-416.el8           AppStream        599 k
 perl-encoding                      x86_64       4:2.22-3.el8               AppStream         68 k
 perl-experimental                  noarch       0.019-2.el8                AppStream         24 k
 perl-inc-latest                    noarch       2:0.500-9.el8              AppStream         25 k
 perl-libnetcfg                     noarch       4:5.26.3-416.el8           AppStream         77 k
 perl-local-lib                     noarch       2.000024-2.el8             AppStream         74 k
 perl-open                          noarch       1.11-416.el8               AppStream         77 k
 perl-perlfaq                       noarch       5.20180605-1.el8           AppStream        386 k
 perl-utils                         noarch       5.26.3-416.el8             AppStream        128 k
 perl-version                       x86_64       6:0.99.24-1.el8            AppStream         67 k
 python3-pyparsing                  noarch       2.1.10-7.el8               BaseOS           142 k
 systemtap-sdt-devel                x86_64       4.2-6.el8                  AppStream         81 k
Installing weak dependencies:
 perl-Encode-Locale                 noarch       1.05-9.el8                 AppStream         21 k

Transaction Summary
===================================================================================================
Install  100 Packages

Total download size: 9.3 M
Installed size: 26 M
Downloading Packages:
(1/100): perl-5.26.3-416.el8.x86_64.rpm                             14 kB/s |  72 kB     00:05    
(2/100): perl-Algorithm-Diff-1.1903-9.el8.noarch.rpm               9.6 kB/s |  52 kB     00:05    
(3/100): perl-Archive-Tar-2.30-1.el8.noarch.rpm                     15 kB/s |  79 kB     00:05    
(4/100): perl-Attribute-Handlers-0.99-416.el8.noarch.rpm           539 kB/s |  88 kB     00:00    
(5/100): perl-B-Debug-1.26-2.el8.noarch.rpm                        199 kB/s |  26 kB     00:00    
(6/100): perl-Archive-Zip-1.60-3.el8.noarch.rpm                    238 kB/s | 108 kB     00:00    
(7/100): perl-CPAN-Meta-Requirements-2.140-396.el8.noarch.rpm      247 kB/s |  37 kB     00:00    
(8/100): perl-CPAN-Meta-YAML-0.018-397.el8.noarch.rpm              271 kB/s |  34 kB     00:00    
(9/100): perl-Compress-Bzip2-2.26-6.el8.x86_64.rpm                 387 kB/s |  72 kB     00:00    
(10/100): perl-Compress-Raw-Bzip2-2.081-1.el8.x86_64.rpm           347 kB/s |  40 kB     00:00    
(11/100): perl-Compress-Raw-Zlib-2.081-1.el8.x86_64.rpm            414 kB/s |  68 kB     00:00    
(12/100): perl-Config-Perl-V-0.30-1.el8.noarch.rpm                 125 kB/s |  22 kB     00:00    
(13/100): perl-CPAN-Meta-2.150010-396.el8.noarch.rpm               166 kB/s | 191 kB     00:01    
(14/100): perl-DB_File-1.842-1.el8.x86_64.rpm                      429 kB/s |  83 kB     00:00    
(15/100): perl-CPAN-2.18-397.el8.noarch.rpm                        412 kB/s | 554 kB     00:01    
(16/100): perl-Data-OptList-0.110-6.el8.noarch.rpm                 185 kB/s |  31 kB     00:00    
(17/100): perl-Data-Section-0.200007-3.el8.noarch.rpm              273 kB/s |  30 kB     00:00    
(18/100): perl-Devel-Peek-1.26-416.el8.x86_64.rpm                  528 kB/s |  93 kB     00:00    
(19/100): perl-Devel-SelfStubber-1.06-416.el8.noarch.rpm           653 kB/s |  75 kB     00:00    
(20/100): perl-Devel-Size-0.81-2.el8.x86_64.rpm                    309 kB/s |  34 kB     00:00    
(21/100): perl-Devel-PPPort-3.36-5.el8.x86_64.rpm                  396 kB/s | 118 kB     00:00    
(22/100): perl-Digest-SHA-6.02-1.el8.x86_64.rpm                    325 kB/s |  66 kB     00:00    
(23/100): perl-Encode-Locale-1.05-9.el8.noarch.rpm                 195 kB/s |  21 kB     00:00    
(24/100): perl-Encode-Detect-1.01-28.el8.x86_64.rpm                475 kB/s |  90 kB     00:00    
(25/100): perl-Env-1.04-395.el8.noarch.rpm                         225 kB/s |  21 kB     00:00    
(26/100): perl-Encode-devel-2.97-3.el8.x86_64.rpm                  261 kB/s |  39 kB     00:00    
(27/100): perl-ExtUtils-Command-7.34-1.el8.noarch.rpm              239 kB/s |  19 kB     00:00    
(28/100): perl-ExtUtils-Embed-1.34-416.el8.noarch.rpm              816 kB/s |  79 kB     00:00    
(29/100): perl-ExtUtils-MM-Utils-7.34-1.el8.noarch.rpm             201 kB/s |  17 kB     00:00    
(30/100): perl-ExtUtils-CBuilder-0.280230-2.el8.noarch.rpm         182 kB/s |  48 kB     00:00    
(31/100): perl-ExtUtils-Install-2.14-4.el8.noarch.rpm              262 kB/s |  46 kB     00:00    
(32/100): perl-ExtUtils-Manifest-1.70-395.el8.noarch.rpm            98 kB/s |  37 kB     00:00    
(33/100): perl-ExtUtils-Miniperl-1.06-416.el8.noarch.rpm           209 kB/s |  76 kB     00:00    
(34/100): perl-File-Fetch-0.56-2.el8.noarch.rpm                    230 kB/s |  33 kB     00:00    
(35/100): perl-ExtUtils-ParseXS-3.35-2.el8.noarch.rpm              225 kB/s |  83 kB     00:00    
(36/100): perl-File-HomeDir-1.002-4.el8.noarch.rpm                 193 kB/s |  61 kB     00:00    
(37/100): perl-File-Which-1.22-2.el8.noarch.rpm                    200 kB/s |  23 kB     00:00    
(38/100): perl-Filter-Simple-0.94-2.el8.noarch.rpm                 231 kB/s |  29 kB     00:00    
(39/100): perl-ExtUtils-MakeMaker-7.34-1.el8.noarch.rpm            253 kB/s | 300 kB     00:01    
(40/100): perl-Filter-1.58-2.el8.x86_64.rpm                        198 kB/s |  82 kB     00:00    
(41/100): perl-IO-Zlib-1.10-416.el8.noarch.rpm                     528 kB/s |  80 kB     00:00    
(42/100): perl-IPC-Cmd-1.02-1.el8.noarch.rpm                       157 kB/s |  43 kB     00:00    
(43/100): perl-IPC-SysV-2.07-397.el8.x86_64.rpm                    140 kB/s |  43 kB     00:00    
(44/100): perl-IPC-System-Simple-1.25-17.el8.noarch.rpm            291 kB/s |  43 kB     00:00    
(45/100): perl-JSON-PP-2.97.001-3.el8.noarch.rpm                   317 kB/s |  68 kB     00:00    
(46/100): perl-IO-Compress-2.081-1.el8.noarch.rpm                  271 kB/s | 258 kB     00:00    
(47/100): perl-Locale-Maketext-Simple-0.21-416.el8.noarch.rpm      479 kB/s |  78 kB     00:00    
(48/100): perl-Locale-Maketext-1.28-396.el8.noarch.rpm             348 kB/s |  99 kB     00:00    
(49/100): perl-MRO-Compat-0.13-4.el8.noarch.rpm                    205 kB/s |  24 kB     00:00    
(50/100): perl-Math-BigInt-FastCalc-0.500.600-6.el8.x86_64.rpm     250 kB/s |  27 kB     00:00    
(51/100): perl-Math-BigRat-0.2614-1.el8.noarch.rpm                 263 kB/s |  40 kB     00:00    
(52/100): perl-Memoize-1.03-416.el8.noarch.rpm                     400 kB/s | 118 kB     00:00    
(53/100): perl-Locale-Codes-3.57-1.el8.noarch.rpm                  335 kB/s | 311 kB     00:00    
(54/100): perl-Module-CoreList-tools-5.20181130-1.el8.noarch.rpm   243 kB/s |  22 kB     00:00    
(55/100): perl-Module-Load-0.32-395.el8.noarch.rpm                 209 kB/s |  19 kB     00:00    
(56/100): perl-Module-CoreList-5.20181130-1.el8.noarch.rpm         283 kB/s |  87 kB     00:00    
(57/100): perl-Module-Load-Conditional-0.68-395.el8.noarch.rpm     197 kB/s |  24 kB     00:00    
(58/100): perl-Module-Loaded-0.08-416.el8.noarch.rpm               517 kB/s |  74 kB     00:00    
(59/100): perl-Module-Metadata-1.000033-395.el8.noarch.rpm         286 kB/s |  44 kB     00:00    
(60/100): perl-Net-Ping-2.55-416.el8.noarch.rpm                    483 kB/s | 101 kB     00:00    
(61/100): perl-Package-Generator-1.106-11.el8.noarch.rpm           237 kB/s |  27 kB     00:00    
(62/100): perl-Params-Check-0.38-395.el8.noarch.rpm                203 kB/s |  24 kB     00:00    
(63/100): perl-Params-Util-1.07-22.el8.x86_64.rpm                  291 kB/s |  44 kB     00:00    
(64/100): perl-Module-Build-0.42.24-5.el8.noarch.rpm               281 kB/s | 273 kB     00:00    
(65/100): perl-Perl-OSType-1.010-396.el8.noarch.rpm                241 kB/s |  29 kB     00:00    
(66/100): perl-PerlIO-via-QuotedPrint-0.08-395.el8.noarch.rpm      150 kB/s |  13 kB     00:00    
(67/100): perl-Pod-Checker-1.73-395.el8.noarch.rpm                 211 kB/s |  33 kB     00:00    
(68/100): perl-Pod-Html-1.22.02-416.el8.noarch.rpm                 422 kB/s |  87 kB     00:00    
(69/100): perl-SelfLoader-1.23-416.el8.noarch.rpm                  518 kB/s |  82 kB     00:00    
(70/100): perl-Pod-Parser-1.63-396.el8.noarch.rpm                  300 kB/s | 108 kB     00:00    
(71/100): perl-Sub-Install-0.928-14.el8.noarch.rpm                 195 kB/s |  27 kB     00:00    
(72/100): perl-Sub-Exporter-0.987-15.el8.noarch.rpm                191 kB/s |  73 kB     00:00    
(73/100): perl-Sys-Syslog-0.35-397.el8.x86_64.rpm                  280 kB/s |  50 kB     00:00    
(74/100): perl-Test-1.30-416.el8.noarch.rpm                        522 kB/s |  89 kB     00:00    
(75/100): perl-Software-License-0.103013-2.el8.noarch.rpm          156 kB/s | 138 kB     00:00    
(76/100): perl-Text-Balanced-2.03-395.el8.noarch.rpm               293 kB/s |  58 kB     00:00    
(77/100): perl-Text-Diff-1.45-2.el8.noarch.rpm                     141 kB/s |  45 kB     00:00    
(78/100): perl-Text-Glob-0.11-4.el8.noarch.rpm                      74 kB/s |  17 kB     00:00    
(79/100): perl-Text-Template-1.51-1.el8.noarch.rpm                 313 kB/s |  64 kB     00:00    
(80/100): perl-Test-Harness-3.42-1.el8.noarch.rpm                  204 kB/s | 279 kB     00:01    
(81/100): perl-Thread-Queue-3.13-1.el8.noarch.rpm                  222 kB/s |  24 kB     00:00    
(82/100): perl-Time-HiRes-1.9758-1.el8.x86_64.rpm                  301 kB/s |  61 kB     00:00    
(83/100): perl-Time-Piece-1.31-416.el8.x86_64.rpm                  504 kB/s |  98 kB     00:00    
(84/100): perl-autodie-2.29-396.el8.noarch.rpm                     244 kB/s |  98 kB     00:00    
(85/100): perl-bignum-0.49-2.el8.noarch.rpm                        186 kB/s |  44 kB     00:00    
(86/100): perl-Unicode-Collate-1.25-2.el8.x86_64.rpm               464 kB/s | 686 kB     00:01    
(87/100): perl-encoding-2.22-3.el8.x86_64.rpm                      332 kB/s |  68 kB     00:00    
(88/100): perl-experimental-0.019-2.el8.noarch.rpm                 226 kB/s |  24 kB     00:00    
(89/100): perl-Test-Simple-1.302135-1.el8.noarch.rpm               153 kB/s | 516 kB     00:03    
(90/100): perl-inc-latest-0.500-9.el8.noarch.rpm                   186 kB/s |  25 kB     00:00    
(91/100): perl-libnetcfg-5.26.3-416.el8.noarch.rpm                 525 kB/s |  77 kB     00:00    
(92/100): perl-local-lib-2.000024-2.el8.noarch.rpm                 320 kB/s |  74 kB     00:00    
(93/100): perl-open-1.11-416.el8.noarch.rpm                        639 kB/s |  77 kB     00:00    
(94/100): perl-devel-5.26.3-416.el8.x86_64.rpm                     378 kB/s | 599 kB     00:01    
(95/100): perl-utils-5.26.3-416.el8.noarch.rpm                     460 kB/s | 128 kB     00:00    
(96/100): perl-version-0.99.24-1.el8.x86_64.rpm                    292 kB/s |  67 kB     00:00    
(97/100): systemtap-sdt-devel-4.2-6.el8.x86_64.rpm                 179 kB/s |  81 kB     00:00    
(98/100): perl-perlfaq-5.20180605-1.el8.noarch.rpm                 479 kB/s | 386 kB     00:00    
(99/100): perl-IO-Tty-1.12-11.el8.x86_64.rpm                       252 kB/s |  48 kB     00:00    
(100/100): python3-pyparsing-2.1.10-7.el8.noarch.rpm                26 kB/s | 142 kB     00:05    
---------------------------------------------------------------------------------------------------
Total                                                              249 kB/s | 9.3 MB     00:38     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                           1/1 
  Installing       : perl-version-6:0.99.24-1.el8.x86_64                                     1/100 
  Installing       : perl-Time-HiRes-1.9758-1.el8.x86_64                                     2/100 
  Installing       : perl-CPAN-Meta-Requirements-2.140-396.el8.noarch                        3/100 
  Installing       : perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch                               4/100 
  Installing       : perl-ExtUtils-Manifest-1.70-395.el8.noarch                              5/100 
  Installing       : perl-Test-Harness-1:3.42-1.el8.noarch                                   6/100 
  Installing       : perl-Module-CoreList-1:5.20181130-1.el8.noarch                          7/100 
  Installing       : perl-Module-Metadata-1.000033-395.el8.noarch                            8/100 
  Installing       : perl-SelfLoader-1.23-416.el8.noarch                                     9/100 
  Installing       : perl-Perl-OSType-1.010-396.el8.noarch                                  10/100 
  Installing       : perl-Module-Load-1:0.32-395.el8.noarch                                 11/100 
  Installing       : perl-JSON-PP-1:2.97.001-3.el8.noarch                                   12/100 
  Installing       : perl-Filter-2:1.58-2.el8.x86_64                                        13/100 
  Installing       : perl-Compress-Raw-Zlib-2.081-1.el8.x86_64                              14/100 
  Installing       : perl-encoding-4:2.22-3.el8.x86_64                                      15/100 
  Installing       : perl-Text-Balanced-2.03-395.el8.noarch                                 16/100 
  Installing       : perl-Net-Ping-2.55-416.el8.noarch                                      17/100 
  Installing       : perl-Sub-Install-0.928-14.el8.noarch                                   18/100 
  Installing       : perl-Pod-Html-1.22.02-416.el8.noarch                                   19/100 
  Installing       : perl-Params-Util-1.07-22.el8.x86_64                                    20/100 
  Installing       : perl-Math-BigRat-0.2614-1.el8.noarch                                   21/100 
  Installing       : perl-Locale-Maketext-1.28-396.el8.noarch                               22/100 
  Installing       : perl-Locale-Maketext-Simple-1:0.21-416.el8.noarch                      23/100 
  Installing       : perl-Params-Check-1:0.38-395.el8.noarch                                24/100 
  Installing       : perl-Module-Load-Conditional-0.68-395.el8.noarch                       25/100 
  Installing       : perl-ExtUtils-Command-1:7.34-1.el8.noarch                              26/100 
  Installing       : perl-Digest-SHA-1:6.02-1.el8.x86_64                                    27/100 
  Installing       : perl-Compress-Raw-Bzip2-2.081-1.el8.x86_64                             28/100 
  Installing       : perl-IO-Compress-2.081-1.el8.noarch                                    29/100 
  Installing       : perl-IO-Zlib-1:1.10-416.el8.noarch                                     30/100 
  Installing       : perl-CPAN-Meta-YAML-0.018-397.el8.noarch                               31/100 
  Installing       : perl-CPAN-Meta-2.150010-396.el8.noarch                                 32/100 
  Installing       : perl-bignum-0.49-2.el8.noarch                                          33/100 
  Installing       : perl-Data-OptList-0.110-6.el8.noarch                                   34/100 
  Installing       : perl-Filter-Simple-0.94-2.el8.noarch                                   35/100 
  Installing       : perl-open-1.11-416.el8.noarch                                          36/100 
  Installing       : perl-Archive-Zip-1.60-3.el8.noarch                                     37/100 
  Installing       : perl-Devel-SelfStubber-1.06-416.el8.noarch                             38/100 
  Installing       : perl-Module-CoreList-tools-1:5.20181130-1.el8.noarch                   39/100 
  Installing       : perl-experimental-0.019-2.el8.noarch                                   40/100 
  Installing       : python3-pyparsing-2.1.10-7.el8.noarch                                  41/100 
  Installing       : systemtap-sdt-devel-4.2-6.el8.x86_64                                   42/100 
  Installing       : perl-utils-5.26.3-416.el8.noarch                                       43/100 
  Installing       : perl-perlfaq-5.20180605-1.el8.noarch                                   44/100 
  Installing       : perl-local-lib-2.000024-2.el8.noarch                                   45/100 
  Installing       : perl-Unicode-Collate-1.25-2.el8.x86_64                                 46/100 
  Installing       : perl-Time-Piece-1.31-416.el8.x86_64                                    47/100 
  Installing       : perl-Thread-Queue-3.13-1.el8.noarch                                    48/100 
  Installing       : perl-Text-Template-1.51-1.el8.noarch                                   49/100 
  Installing       : perl-Text-Glob-0.11-4.el8.noarch                                       50/100 
  Installing       : perl-Test-Simple-1:1.302135-1.el8.noarch                               51/100 
  Installing       : perl-Test-1.30-416.el8.noarch                                          52/100 
  Installing       : perl-Sys-Syslog-0.35-397.el8.x86_64                                    53/100 
  Installing       : perl-Pod-Parser-1.63-396.el8.noarch                                    54/100 
  Installing       : perl-Pod-Checker-4:1.73-395.el8.noarch                                 55/100 
  Installing       : perl-PerlIO-via-QuotedPrint-0.08-395.el8.noarch                        56/100 
  Installing       : perl-Package-Generator-1.106-11.el8.noarch                             57/100 
  Installing       : perl-Sub-Exporter-0.987-15.el8.noarch                                  58/100 
  Installing       : perl-Module-Loaded-1:0.08-416.el8.noarch                               59/100 
  Installing       : perl-Memoize-1.03-416.el8.noarch                                       60/100 
  Installing       : perl-Math-BigInt-FastCalc-0.500.600-6.el8.x86_64                       61/100 
  Installing       : perl-MRO-Compat-0.13-4.el8.noarch                                      62/100 
  Installing       : perl-Data-Section-0.200007-3.el8.noarch                                63/100 
  Installing       : perl-Software-License-0.103013-2.el8.noarch                            64/100 
  Installing       : perl-Locale-Codes-3.57-1.el8.noarch                                    65/100 
  Installing       : perl-IPC-System-Simple-1.25-17.el8.noarch                              66/100 
  Installing       : perl-autodie-2.29-396.el8.noarch                                       67/100 
  Installing       : perl-IPC-SysV-2.07-397.el8.x86_64                                      68/100 
  Installing       : perl-File-Which-1.22-2.el8.noarch                                      69/100 
  Installing       : perl-File-HomeDir-1.002-4.el8.noarch                                   70/100 
  Installing       : perl-ExtUtils-MM-Utils-1:7.34-1.el8.noarch                             71/100 
  Installing       : perl-IPC-Cmd-2:1.02-1.el8.noarch                                       72/100 
  Installing       : perl-File-Fetch-0.56-2.el8.noarch                                      73/100 
  Installing       : perl-Env-1.04-395.el8.noarch                                           74/100 
  Installing       : perl-Encode-Locale-1.05-9.el8.noarch                                   75/100 
  Installing       : perl-devel-4:5.26.3-416.el8.x86_64                                     76/100 
  Installing       : perl-ExtUtils-Install-2.14-4.el8.noarch                                77/100 
  Installing       : perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch                            78/100 
  Installing       : perl-ExtUtils-CBuilder-1:0.280230-2.el8.noarch                         79/100 
  Installing       : perl-ExtUtils-Embed-1.34-416.el8.noarch                                80/100 
  Installing       : perl-ExtUtils-Miniperl-1.06-416.el8.noarch                             81/100 
  Installing       : perl-libnetcfg-4:5.26.3-416.el8.noarch                                 82/100 
  Installing       : perl-inc-latest-2:0.500-9.el8.noarch                                   83/100 
  Installing       : perl-Module-Build-2:0.42.24-5.el8.noarch                               84/100 
  Installing       : perl-Encode-devel-4:2.97-3.el8.x86_64                                  85/100 
  Installing       : perl-Devel-Size-0.81-2.el8.x86_64                                      86/100 
  Installing       : perl-Devel-Peek-1.26-416.el8.x86_64                                    87/100 
  Installing       : perl-Devel-PPPort-3.36-5.el8.x86_64                                    88/100 
  Installing       : perl-DB_File-1.842-1.el8.x86_64                                        89/100 
  Installing       : perl-Config-Perl-V-0.30-1.el8.noarch                                   90/100 
  Installing       : perl-Compress-Bzip2-2.26-6.el8.x86_64                                  91/100 
  Installing       : perl-B-Debug-1.26-2.el8.noarch                                         92/100 
  Installing       : perl-Attribute-Handlers-0.99-416.el8.noarch                            93/100 
  Installing       : perl-Algorithm-Diff-1.1903-9.el8.noarch                                94/100 
  Installing       : perl-Text-Diff-1.45-2.el8.noarch                                       95/100 
  Installing       : perl-Archive-Tar-2.30-1.el8.noarch                                     96/100 
  Installing       : perl-CPAN-2.18-397.el8.noarch                                          97/100 
  Installing       : perl-4:5.26.3-416.el8.x86_64                                           98/100 
  Installing       : perl-IO-Tty-1.12-11.el8.x86_64                                         99/100 
  Installing       : perl-Encode-Detect-1.01-28.el8.x86_64                                 100/100 
  Running scriptlet: perl-Encode-Detect-1.01-28.el8.x86_64                                 100/100 
  Verifying        : perl-4:5.26.3-416.el8.x86_64                                            1/100 
  Verifying        : perl-Algorithm-Diff-1.1903-9.el8.noarch                                 2/100 
  Verifying        : perl-Archive-Tar-2.30-1.el8.noarch                                      3/100 
  Verifying        : perl-Archive-Zip-1.60-3.el8.noarch                                      4/100 
  Verifying        : perl-Attribute-Handlers-0.99-416.el8.noarch                             5/100 
  Verifying        : perl-B-Debug-1.26-2.el8.noarch                                          6/100 
  Verifying        : perl-CPAN-2.18-397.el8.noarch                                           7/100 
  Verifying        : perl-CPAN-Meta-2.150010-396.el8.noarch                                  8/100 
  Verifying        : perl-CPAN-Meta-Requirements-2.140-396.el8.noarch                        9/100 
  Verifying        : perl-CPAN-Meta-YAML-0.018-397.el8.noarch                               10/100 
  Verifying        : perl-Compress-Bzip2-2.26-6.el8.x86_64                                  11/100 
  Verifying        : perl-Compress-Raw-Bzip2-2.081-1.el8.x86_64                             12/100 
  Verifying        : perl-Compress-Raw-Zlib-2.081-1.el8.x86_64                              13/100 
  Verifying        : perl-Config-Perl-V-0.30-1.el8.noarch                                   14/100 
  Verifying        : perl-DB_File-1.842-1.el8.x86_64                                        15/100 
  Verifying        : perl-Data-OptList-0.110-6.el8.noarch                                   16/100 
  Verifying        : perl-Data-Section-0.200007-3.el8.noarch                                17/100 
  Verifying        : perl-Devel-PPPort-3.36-5.el8.x86_64                                    18/100 
  Verifying        : perl-Devel-Peek-1.26-416.el8.x86_64                                    19/100 
  Verifying        : perl-Devel-SelfStubber-1.06-416.el8.noarch                             20/100 
  Verifying        : perl-Devel-Size-0.81-2.el8.x86_64                                      21/100 
  Verifying        : perl-Digest-SHA-1:6.02-1.el8.x86_64                                    22/100 
  Verifying        : perl-Encode-Detect-1.01-28.el8.x86_64                                  23/100 
  Verifying        : perl-Encode-Locale-1.05-9.el8.noarch                                   24/100 
  Verifying        : perl-Encode-devel-4:2.97-3.el8.x86_64                                  25/100 
  Verifying        : perl-Env-1.04-395.el8.noarch                                           26/100 
  Verifying        : perl-ExtUtils-CBuilder-1:0.280230-2.el8.noarch                         27/100 
  Verifying        : perl-ExtUtils-Command-1:7.34-1.el8.noarch                              28/100 
  Verifying        : perl-ExtUtils-Embed-1.34-416.el8.noarch                                29/100 
  Verifying        : perl-ExtUtils-Install-2.14-4.el8.noarch                                30/100 
  Verifying        : perl-ExtUtils-MM-Utils-1:7.34-1.el8.noarch                             31/100 
  Verifying        : perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch                            32/100 
  Verifying        : perl-ExtUtils-Manifest-1.70-395.el8.noarch                             33/100 
  Verifying        : perl-ExtUtils-Miniperl-1.06-416.el8.noarch                             34/100 
  Verifying        : perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch                              35/100 
  Verifying        : perl-File-Fetch-0.56-2.el8.noarch                                      36/100 
  Verifying        : perl-File-HomeDir-1.002-4.el8.noarch                                   37/100 
  Verifying        : perl-File-Which-1.22-2.el8.noarch                                      38/100 
  Verifying        : perl-Filter-2:1.58-2.el8.x86_64                                        39/100 
  Verifying        : perl-Filter-Simple-0.94-2.el8.noarch                                   40/100 
  Verifying        : perl-IO-Compress-2.081-1.el8.noarch                                    41/100 
  Verifying        : perl-IO-Zlib-1:1.10-416.el8.noarch                                     42/100 
  Verifying        : perl-IPC-Cmd-2:1.02-1.el8.noarch                                       43/100 
  Verifying        : perl-IPC-SysV-2.07-397.el8.x86_64                                      44/100 
  Verifying        : perl-IPC-System-Simple-1.25-17.el8.noarch                              45/100 
  Verifying        : perl-JSON-PP-1:2.97.001-3.el8.noarch                                   46/100 
  Verifying        : perl-Locale-Codes-3.57-1.el8.noarch                                    47/100 
  Verifying        : perl-Locale-Maketext-1.28-396.el8.noarch                               48/100 
  Verifying        : perl-Locale-Maketext-Simple-1:0.21-416.el8.noarch                      49/100 
  Verifying        : perl-MRO-Compat-0.13-4.el8.noarch                                      50/100 
  Verifying        : perl-Math-BigInt-FastCalc-0.500.600-6.el8.x86_64                       51/100 
  Verifying        : perl-Math-BigRat-0.2614-1.el8.noarch                                   52/100 
  Verifying        : perl-Memoize-1.03-416.el8.noarch                                       53/100 
  Verifying        : perl-Module-Build-2:0.42.24-5.el8.noarch                               54/100 
  Verifying        : perl-Module-CoreList-1:5.20181130-1.el8.noarch                         55/100 
  Verifying        : perl-Module-CoreList-tools-1:5.20181130-1.el8.noarch                   56/100 
  Verifying        : perl-Module-Load-1:0.32-395.el8.noarch                                 57/100 
  Verifying        : perl-Module-Load-Conditional-0.68-395.el8.noarch                       58/100 
  Verifying        : perl-Module-Loaded-1:0.08-416.el8.noarch                               59/100 
  Verifying        : perl-Module-Metadata-1.000033-395.el8.noarch                           60/100 
  Verifying        : perl-Net-Ping-2.55-416.el8.noarch                                      61/100 
  Verifying        : perl-Package-Generator-1.106-11.el8.noarch                             62/100 
  Verifying        : perl-Params-Check-1:0.38-395.el8.noarch                                63/100 
  Verifying        : perl-Params-Util-1.07-22.el8.x86_64                                    64/100 
  Verifying        : perl-Perl-OSType-1.010-396.el8.noarch                                  65/100 
  Verifying        : perl-PerlIO-via-QuotedPrint-0.08-395.el8.noarch                        66/100 
  Verifying        : perl-Pod-Checker-4:1.73-395.el8.noarch                                 67/100 
  Verifying        : perl-Pod-Html-1.22.02-416.el8.noarch                                   68/100 
  Verifying        : perl-Pod-Parser-1.63-396.el8.noarch                                    69/100 
  Verifying        : perl-SelfLoader-1.23-416.el8.noarch                                    70/100 
  Verifying        : perl-Software-License-0.103013-2.el8.noarch                            71/100 
  Verifying        : perl-Sub-Exporter-0.987-15.el8.noarch                                  72/100 
  Verifying        : perl-Sub-Install-0.928-14.el8.noarch                                   73/100 
  Verifying        : perl-Sys-Syslog-0.35-397.el8.x86_64                                    74/100 
  Verifying        : perl-Test-1.30-416.el8.noarch                                          75/100 
  Verifying        : perl-Test-Harness-1:3.42-1.el8.noarch                                  76/100 
  Verifying        : perl-Test-Simple-1:1.302135-1.el8.noarch                               77/100 
  Verifying        : perl-Text-Balanced-2.03-395.el8.noarch                                 78/100 
  Verifying        : perl-Text-Diff-1.45-2.el8.noarch                                       79/100 
  Verifying        : perl-Text-Glob-0.11-4.el8.noarch                                       80/100 
  Verifying        : perl-Text-Template-1.51-1.el8.noarch                                   81/100 
  Verifying        : perl-Thread-Queue-3.13-1.el8.noarch                                    82/100 
  Verifying        : perl-Time-HiRes-1.9758-1.el8.x86_64                                    83/100 
  Verifying        : perl-Time-Piece-1.31-416.el8.x86_64                                    84/100 
  Verifying        : perl-Unicode-Collate-1.25-2.el8.x86_64                                 85/100 
  Verifying        : perl-autodie-2.29-396.el8.noarch                                       86/100 
  Verifying        : perl-bignum-0.49-2.el8.noarch                                          87/100 
  Verifying        : perl-devel-4:5.26.3-416.el8.x86_64                                     88/100 
  Verifying        : perl-encoding-4:2.22-3.el8.x86_64                                      89/100 
  Verifying        : perl-experimental-0.019-2.el8.noarch                                   90/100 
  Verifying        : perl-inc-latest-2:0.500-9.el8.noarch                                   91/100 
  Verifying        : perl-libnetcfg-4:5.26.3-416.el8.noarch                                 92/100 
  Verifying        : perl-local-lib-2.000024-2.el8.noarch                                   93/100 
  Verifying        : perl-open-1.11-416.el8.noarch                                          94/100 
  Verifying        : perl-perlfaq-5.20180605-1.el8.noarch                                   95/100 
  Verifying        : perl-utils-5.26.3-416.el8.noarch                                       96/100 
  Verifying        : perl-version-6:0.99.24-1.el8.x86_64                                    97/100 
  Verifying        : systemtap-sdt-devel-4.2-6.el8.x86_64                                   98/100 
  Verifying        : python3-pyparsing-2.1.10-7.el8.noarch                                  99/100 
  Verifying        : perl-IO-Tty-1.12-11.el8.x86_64                                        100/100 
Installed products updated.

Installed:
  perl-4:5.26.3-416.el8.x86_64                                                                     
  perl-Algorithm-Diff-1.1903-9.el8.noarch                                                          
  perl-Archive-Tar-2.30-1.el8.noarch                                                               
  perl-Archive-Zip-1.60-3.el8.noarch                                                               
  perl-Attribute-Handlers-0.99-416.el8.noarch                                                      
  perl-B-Debug-1.26-2.el8.noarch                                                                   
  perl-CPAN-2.18-397.el8.noarch                                                                    
  perl-CPAN-Meta-2.150010-396.el8.noarch                                                           
  perl-CPAN-Meta-Requirements-2.140-396.el8.noarch                                                 
  perl-CPAN-Meta-YAML-0.018-397.el8.noarch                                                         
  perl-Compress-Bzip2-2.26-6.el8.x86_64                                                            
  perl-Compress-Raw-Bzip2-2.081-1.el8.x86_64                                                       
  perl-Compress-Raw-Zlib-2.081-1.el8.x86_64                                                        
  perl-Config-Perl-V-0.30-1.el8.noarch                                                             
  perl-DB_File-1.842-1.el8.x86_64                                                                  
  perl-Data-OptList-0.110-6.el8.noarch                                                             
  perl-Data-Section-0.200007-3.el8.noarch                                                          
  perl-Devel-PPPort-3.36-5.el8.x86_64                                                              
  perl-Devel-Peek-1.26-416.el8.x86_64                                                              
  perl-Devel-SelfStubber-1.06-416.el8.noarch                                                       
  perl-Devel-Size-0.81-2.el8.x86_64                                                                
  perl-Digest-SHA-1:6.02-1.el8.x86_64                                                              
  perl-Encode-Detect-1.01-28.el8.x86_64                                                            
  perl-Encode-Locale-1.05-9.el8.noarch                                                             
  perl-Encode-devel-4:2.97-3.el8.x86_64                                                            
  perl-Env-1.04-395.el8.noarch                                                                     
  perl-ExtUtils-CBuilder-1:0.280230-2.el8.noarch                                                   
  perl-ExtUtils-Command-1:7.34-1.el8.noarch                                                        
  perl-ExtUtils-Embed-1.34-416.el8.noarch                                                          
  perl-ExtUtils-Install-2.14-4.el8.noarch                                                          
  perl-ExtUtils-MM-Utils-1:7.34-1.el8.noarch                                                       
  perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch                                                      
  perl-ExtUtils-Manifest-1.70-395.el8.noarch                                                       
  perl-ExtUtils-Miniperl-1.06-416.el8.noarch                                                       
  perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch                                                        
  perl-File-Fetch-0.56-2.el8.noarch                                                                
  perl-File-HomeDir-1.002-4.el8.noarch                                                             
  perl-File-Which-1.22-2.el8.noarch                                                                
  perl-Filter-2:1.58-2.el8.x86_64                                                                  
  perl-Filter-Simple-0.94-2.el8.noarch                                                             
  perl-IO-Compress-2.081-1.el8.noarch                                                              
  perl-IO-Tty-1.12-11.el8.x86_64                                                                   
  perl-IO-Zlib-1:1.10-416.el8.noarch                                                               
  perl-IPC-Cmd-2:1.02-1.el8.noarch                                                                 
  perl-IPC-SysV-2.07-397.el8.x86_64                                                                
  perl-IPC-System-Simple-1.25-17.el8.noarch                                                        
  perl-JSON-PP-1:2.97.001-3.el8.noarch                                                             
  perl-Locale-Codes-3.57-1.el8.noarch                                                              
  perl-Locale-Maketext-1.28-396.el8.noarch                                                         
  perl-Locale-Maketext-Simple-1:0.21-416.el8.noarch                                                
  perl-MRO-Compat-0.13-4.el8.noarch                                                                
  perl-Math-BigInt-FastCalc-0.500.600-6.el8.x86_64                                                 
  perl-Math-BigRat-0.2614-1.el8.noarch                                                             
  perl-Memoize-1.03-416.el8.noarch                                                                 
  perl-Module-Build-2:0.42.24-5.el8.noarch                                                         
  perl-Module-CoreList-1:5.20181130-1.el8.noarch                                                   
  perl-Module-CoreList-tools-1:5.20181130-1.el8.noarch                                             
  perl-Module-Load-1:0.32-395.el8.noarch                                                           
  perl-Module-Load-Conditional-0.68-395.el8.noarch                                                 
  perl-Module-Loaded-1:0.08-416.el8.noarch                                                         
  perl-Module-Metadata-1.000033-395.el8.noarch                                                     
  perl-Net-Ping-2.55-416.el8.noarch                                                                
  perl-Package-Generator-1.106-11.el8.noarch                                                       
  perl-Params-Check-1:0.38-395.el8.noarch                                                          
  perl-Params-Util-1.07-22.el8.x86_64                                                              
  perl-Perl-OSType-1.010-396.el8.noarch                                                            
  perl-PerlIO-via-QuotedPrint-0.08-395.el8.noarch                                                  
  perl-Pod-Checker-4:1.73-395.el8.noarch                                                           
  perl-Pod-Html-1.22.02-416.el8.noarch                                                             
  perl-Pod-Parser-1.63-396.el8.noarch                                                              
  perl-SelfLoader-1.23-416.el8.noarch                                                              
  perl-Software-License-0.103013-2.el8.noarch                                                      
  perl-Sub-Exporter-0.987-15.el8.noarch                                                            
  perl-Sub-Install-0.928-14.el8.noarch                                                             
  perl-Sys-Syslog-0.35-397.el8.x86_64                                                              
  perl-Test-1.30-416.el8.noarch                                                                    
  perl-Test-Harness-1:3.42-1.el8.noarch                                                            
  perl-Test-Simple-1:1.302135-1.el8.noarch                                                         
  perl-Text-Balanced-2.03-395.el8.noarch                                                           
  perl-Text-Diff-1.45-2.el8.noarch                                                                 
  perl-Text-Glob-0.11-4.el8.noarch                                                                 
  perl-Text-Template-1.51-1.el8.noarch                                                             
  perl-Thread-Queue-3.13-1.el8.noarch                                                              
  perl-Time-HiRes-1.9758-1.el8.x86_64                                                              
  perl-Time-Piece-1.31-416.el8.x86_64                                                              
  perl-Unicode-Collate-1.25-2.el8.x86_64                                                           
  perl-autodie-2.29-396.el8.noarch                                                                 
  perl-bignum-0.49-2.el8.noarch                                                                    
  perl-devel-4:5.26.3-416.el8.x86_64                                                               
  perl-encoding-4:2.22-3.el8.x86_64                                                                
  perl-experimental-0.019-2.el8.noarch                                                             
  perl-inc-latest-2:0.500-9.el8.noarch                                                             
  perl-libnetcfg-4:5.26.3-416.el8.noarch                                                           
  perl-local-lib-2.000024-2.el8.noarch                                                             
  perl-open-1.11-416.el8.noarch                                                                    
  perl-perlfaq-5.20180605-1.el8.noarch                                                             
  perl-utils-5.26.3-416.el8.noarch                                                                 
  perl-version-6:0.99.24-1.el8.x86_64                                                              
  python3-pyparsing-2.1.10-7.el8.noarch                                                            
  systemtap-sdt-devel-4.2-6.el8.x86_64                                                             

Complete!
[root@student ~]# rpm -U webmin-1.960-1.noarch.rpm
warning: webmin-1.960-1.noarch.rpm: Header V4 DSA/SHA1 Signature, key ID 11f63c51: NOKEY
Operating system is CentOS Linux
Webmin install complete. You can now login to https://student:10000/
as root with your root password.


# search and install samba including the samba doc pdf files
root@student ~]# rpm -qa | grep samba
samba-common-4.11.2-13.el8.noarch
samba-client-libs-4.11.2-13.el8.x86_64
samba-common-libs-4.11.2-13.el8.x86_64


### info about installing samba, to check later - https://www.samba.org/samba/docs/using_samba/ch02.html

