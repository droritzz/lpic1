# !/usr/bin/env bash
#############################
#owner: droritzz
#puprose: answering questions from the LPIC1
#date: 11/10/20
###########################
#schedule two jobs with at, display the at queue and remove the job

root@vaiolabs:/home/student# at 13:10
warning: commands will be executed using /bin/sh
at> date
at> <EOT>
job 5 at Wed Nov 18 13:10:00 2020
root@vaiolabs:/home/student# at 13:11
warning: commands will be executed using /bin/sh
at> sleep 10
at> <EOT>
job 6 at Wed Nov 18 13:11:00 2020
root@vaiolabs:/home/student# atq
6	Wed Nov 18 13:11:00 2020 a root
5	Wed Nov 18 13:10:00 2020 a root
root@vaiolabs:/home/student# atrm 6

#as normal user, use crontab -e to schedule a script to run every four minutes

student@vaiolabs:/etc$ crontab -e
crontab: installing new crontab
student@vaiolabs:/etc$ crontab -l
# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').
# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command

*/4 * * * * date
student@vaiolabs:/etc$ 

#as root, display the crontab file of your normal user

root@vaiolabs:/etc# crontab -l
no crontab for root
root@vaiolabs:/etc# crontab -l -u student
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').
#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command

*/4 * * * * date
root@vaiolabs:/etc#

#as the normal user again, remove your crontab file

root@vaiolabs:/etc/cron.daily# exit
exit
student@vaiolabs:/etc$ crontab -r 
student@vaiolabs:/etc$ crontab -l
no crontab for student
student@vaiolabs:/etc$ 

#take a look at the cron files and directories in /etc und UNDERSTAND!! them :) what is the run-parts command doing?
student@vaiolabs:/etc$ cat anacrontab 
# /etc/anacrontab: configuration file for anacron

# See anacron(8) and anacrontab(5) for details.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
HOME=/root
LOGNAME=root

# These replace cron's entries
1	5	cron.daily	run-parts --report /etc/cron.daily
7	10	cron.weekly	run-parts --report /etc/cron.weekly
@monthly	15	cron.monthly	run-parts --report /etc/cron.monthly
student@vaiolabs:/etc$ 


run-parts takes a directory as an argument. It will run every script in that directory.

